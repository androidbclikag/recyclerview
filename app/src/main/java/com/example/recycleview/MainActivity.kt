package com.example.recycleview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private  lateinit var itemList: MutableList<ItemModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        linearLayoutManager = LinearLayoutManager(this)
        viewAdapter = RecyclerViewAdapter(itemList)

    }
    private fun init(){

        val firstAuthor = ItemModel(R.mipmap.aka_morchiladze, "აკა მორჩილაძე", "ქართველი პოსტმოდერნისტი მწერალი, ისტორიკოსი და სცენარისტი. ლიტერატურული პრემია „საბას“ ექვსგზის ლაურეატი.")
        val secondAuthor = ItemModel(R.mipmap.dato_turashvili, "დათო ტურაშვილი", "დათო ტურაშვილმა დაამთავრა თბილისის 57-ე საშუალო სკოლა. სწავლობდა თსუ-ის ფილოლოგიის ფაკულტეტზე.")
        val thirdAuthor = ItemModel(R.mipmap.nodar_dumbadze, "ნოდარ დუმბაძე", " ქართველი მწერალი და სცენარისტი, სსრკ-ის მწერალთა კავშირის გამგეობის წევრი, შოთა რუსთაველის სახელობის სახელმწიფო პრემიის ლაურეატი (1975).")
        val fourthAuthor = ItemModel(R.mipmap.revaz_inanishvili, "რევაზ ინანიშვილი", "რევაზ ინანიშვილი დაიბადა 1926 წლის 20 დეკემბერს, საგარეჯოს მუნიციპალიტეტის სოფელ ხაშმში, მოსამსახურის ოჯახში. მშობლიურ სოფელში ხუთი კლასი დაამთავრა.")
        itemList.add(firstAuthor)
        itemList.add(secondAuthor)
        itemList.add(thirdAuthor)
        itemList.add(fourthAuthor)
    }

}