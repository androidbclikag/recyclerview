package com.example.recycleview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.linear_layout.view.*

class RecyclerViewAdapter(private val dataSet: MutableList<ItemModel>) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    inner class MyViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        fun onBind() {
            val items = dataSet[adapterPosition]
            itemView.titleTextView.text = items.title
            itemView.messageTextView.text = items.message
            itemView.imageView.setImageResource(items.image)
        }
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerViewAdapter.MyViewHolder {
        val layout =
            LayoutInflater.from(parent.context).inflate(R.layout.linear_layout, parent, false)
        return MyViewHolder(layout)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()

    }

}